db.createUser(
    {
        user: "admin",
        pwd: "123",
        roles: [
            {
                role: "readWrite",
                db: "stores"
            }
        ]
    }
);

db.stores.insert([
    {
        latitud:  -34.589208,
        longitud: -58.409799,
        address:  "Santa Fe 3181, Buenos Aires",
    },
    {
        latitud:  -34.5611599,
        longitud: -58.4577643,
        address:  "Av cabildo 2202, Buenos Aires",
    },
    {
        latitud:  -34.6054508,
        longitud: -58.4058448,
        address:  "Valentin Gomez 2813, Buenos Aires",
    },
    {
        latitud:  -34.6030997,
        longitud: -58.4190461,
        address:  "Av Corrientes 3881, Buenos Aires",
    },
    {
        latitud:  -34.6039035,
        longitud: -58.4105446,
        address:  "Av Corrientes 3217, Buenos Aires",
    },
    {
        latitud:  -34.6035589,
        longitud: -58.3774422,
        address:  "Av Corrientes 756, Buenos Aires",
    },
    {
        latitud:  -34.619678,
        longitud: -58.439186,
        address:  "Av Rivadavia 5216, Buenos Aires",
    },
    {
        latitud: -34.5946478,
        longitud: -58.4011746,
        address:  "Santa Fe 2401, Buenos Aires",
    }
])