package usecase

import (
	"context"
	"gitlab.com/leancomp/store-challenge/models"
)

type Usecase interface {
	GetByID(ctx context.Context, id models.ID) (*models.Store, error)
	GetClosest(ctx context.Context, lat, lng float64) (*models.Store, error)
	Store(ctx context.Context, s *models.Store) error
}
