package usecase

import(
	"context"
	"testing"
	"time"

	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/mock"

	"gitlab.com/leancomp/store-challenge/models"
	"gitlab.com/leancomp/store-challenge/repository/mocks"
)


func TestStore(t *testing.T) {
	mockStoreRepo := new(mocks.StoreRepository)
	mockStore := models.Store{
		Latitud:  -34.72634,
		Longitud: -56.178263,
		Address:  "Calle falsa 123, Buenos Aires",
	}

	t.Run("success", func(t *testing.T) {
		tempMockStore := mockStore

		mockStoreRepo.On("GetByAddress", mock.Anything, mock.AnythingOfType("string")).Return(nil, models.ErrNotFound).Once()
		mockStoreRepo.On("Store", mock.Anything, mock.AnythingOfType("*models.Store")).Return(nil).Once()

		u := NewStoreUsecase(mockStoreRepo, time.Second*2)

		err := u.Store(context.TODO(), &tempMockStore)

		assert.NoError(t, err)
		assert.Equal(t, mockStore.Address, tempMockStore.Address)
		mockStoreRepo.AssertExpectations(t)
	})
	t.Run("existing-address", func(t *testing.T) {
		existingStore := mockStore
		mockStoreRepo.On("GetByAddress", mock.Anything, mock.AnythingOfType("string")).Return(&existingStore, nil).Once()

		u := NewStoreUsecase(mockStoreRepo, time.Second*2)

		err := u.Store(context.TODO(), &mockStore)

		assert.Error(t, err)
		mockStoreRepo.AssertExpectations(t)
	})
}

func TestClosestStore(t *testing.T) {
	mockStoreRepo := new(mocks.StoreRepository)
	referencePosition := struct {	//Larrea 1106
		lat, lng float64
	} {
		lat: -34.5944341,
		lng: -58.4011121,
	}
	closestStore := models.Store{	//Fravega Esquina
		ID:       models.ID("1"),
		Latitud: -34.5946478,
		Longitud: -58.4011746,
		Address:  "Santa Fe 2401, Buenos Aires",
	}

	mockStores := []*models.Store{
		&closestStore,
		{
			ID:       models.ID("11"),
			Latitud:  -34.589208,
			Longitud: -58.409799,
			Address:  "Santa Fe 3181, Buenos Aires",
		},
		{
			ID:       models.ID("13"),
			Latitud:  -34.5611599,
			Longitud: -58.4577643,
			Address:  "Av cabildo 2202, Buenos Aires",
		},
		{
			ID:       models.ID("14"),
			Latitud:  -34.6054508,
			Longitud: -58.4058448,
			Address:  "Valentin Gomez 2813, Buenos Aires",
		},
		{
			ID:       models.ID("15"),
			Latitud:  -34.6030997,
			Longitud: -58.4190461,
			Address:  "Av Corrientes 3881, Buenos Aires",
		},
		{
			ID:       models.ID("16"),
			Latitud:  -34.6039035,
			Longitud: -58.4105446,
			Address:  "Av Corrientes 3217, Buenos Aires",
		},
		{
			ID:       models.ID("17"),
			Latitud:  -34.6035589,
			Longitud: -58.3774422,
			Address:  "Av Corrientes 756, Buenos Aires",
		},
		{
			ID:       models.ID("17"),
			Latitud:  -34.619678,
			Longitud: -58.439186,
			Address:  "Av Rivadavia 5216, Buenos Aires",
		},
	}

	t.Run("success", func(t *testing.T) {
		mockStoreRepo.On("GetAll", mock.Anything).Return(mockStores, nil).Once()

		u := NewStoreUsecase(mockStoreRepo, time.Second*2)
		var suc *models.Store
		suc, err := u.GetClosest(context.TODO(), referencePosition.lat, referencePosition.lng)

		assert.NoError(t, err)
		assert.Equal(t, suc.ID, closestStore.ID)
		mockStoreRepo.AssertExpectations(t)
	})

}
