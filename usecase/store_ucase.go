package usecase

import (
	"context"
	"sort"
	"time"

	"gitlab.com/leancomp/store-challenge/models"
	"gitlab.com/leancomp/store-challenge/repository"
	"gitlab.com/leancomp/store-challenge/usecase/utils"
)

type storeUsecase struct {
	storeRepository repository.StoreRepository
	contextTimeout  time.Duration
}

func (s storeUsecase) GetByID(c context.Context, id models.ID) (*models.Store, error) {
	ctx, cancel := context.WithTimeout(c, s.contextTimeout)
	defer cancel()

	return s.storeRepository.GetByID(ctx, id)
}

func (s storeUsecase) GetClosest(c context.Context, lat, lng float64) (*models.Store, error) {
	ctx, cancel := context.WithTimeout(c, s.contextTimeout)
	defer cancel()

	sucursales, err := s.storeRepository.GetAll(ctx)
	if err != nil {
		return nil, err
	}

	s.SortStoresByDistanceTo(sucursales, lat, lng)
	return sucursales[0], nil
}

func (s storeUsecase) Store(c context.Context, m *models.Store) error {
	ctx, cancel := context.WithTimeout(c, s.contextTimeout)
	defer cancel()

	existedStore, _ := s.storeRepository.GetByAddress(ctx, m.Address)
	if existedStore != nil {
		return models.ErrConflict
	}

	m.ID = models.NewID()
	return s.storeRepository.Store(ctx, m)
}

func (s storeUsecase) SortStoresByDistanceTo(stores []*models.Store, lat, lng float64) {
	var distancesMap = make(map[models.ID]float64, len(stores))

	for _, s := range stores {
		distancesMap[s.ID] = utils.Distance(lat, lng, s.Latitud, s.Longitud)
	}

	sort.SliceStable(stores, func(i, j int) bool {
		return distancesMap[stores[i].ID] < distancesMap[stores[j].ID]
	})
}

func NewStoreUsecase(r repository.StoreRepository, timeout time.Duration) Usecase {
	return &storeUsecase{
		storeRepository: r,
		contextTimeout:  timeout,
	}
}