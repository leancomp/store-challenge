# Go parameters
BINARY_NAME=main

all: test build

build:
	go build -o $(BINARY_NAME) -v
test:
	go test -v ./...
swagger:
	go run github.com/swaggo/swag/cmd/swag init
clean:
	go clean
	rm -f $(BINARY_NAME)
run:
	docker-compose up api
deps:
	go mod download
docker-compose-build:
	docker-compose up --build api