package config

import (
	"fmt"
	"os"
	"strconv"
)

type AppConfig struct {
	MongoDb struct {
		Host           string
		Database       string
		User		   string
		Pass	       string
		ConnectionPool int
	}
	ApiPort	int
}

func LoadConfig() *AppConfig {
	return &AppConfig{
		ApiPort: getInt("API_PORT"),
		MongoDb: struct {
			Host           string
			Database       string
			User		   string
			Pass	       string
			ConnectionPool int
		}{
			Host:           get("MONGODB_HOST"),
			Database:       get("MONGODB_DATABASE"),
			User:       	get("MONGODB_USER"),
			Pass:       	get("MONGODB_PASS"),
			ConnectionPool: getInt("MONGODB_CONNECTION_POOL")},
	}
}

func getInt(env string) int {
	val, err := strconv.Atoi(get(env))
	if err != nil {
		panic(err)
	}
	return val
}

func get(env string) string {
	if val := os.Getenv(env); val != "" {
		return val
	}
	panic(fmt.Sprintf("Environment variable %s is required", env))
}

