package repository

import (
	"context"

	"github.com/juju/mgosession"
	"gitlab.com/leancomp/store-challenge/models"
	"gopkg.in/mgo.v2"
	"gopkg.in/mgo.v2/bson"
)

const COLLECTION = "stores"

type mongoStoreRepository struct {
	pool *mgosession.Pool
	db   string
}

func NewMongoStoreRepository(p *mgosession.Pool, db string) *mongoStoreRepository {
	return &mongoStoreRepository{
		pool: p,
		db:   db,
	}
}

func (r mongoStoreRepository) GetAll(ctx context.Context) ([]*models.Store, error) {
	var d []*models.Store
	session := r.pool.Session(nil)
	coll := session.DB(r.db).C(COLLECTION)
	err := coll.Find(nil).All(&d)

	switch err {
	case nil:
		return d, nil
	case mgo.ErrNotFound:
		return nil, models.ErrNotFound
	default:
		return nil, err
	}
}

func (r mongoStoreRepository) GetByID(ctx context.Context, id models.ID) (*models.Store, error) {
	result := models.Store{}
	session := r.pool.Session(nil)
	coll := session.DB(r.db).C(COLLECTION)
	err := coll.Find(bson.M{"_id": id}).One(&result)

	switch err {
	case nil:
		return &result, nil
	default:
		return nil, models.ErrNotFound
	}
}

func (r mongoStoreRepository) GetByAddress(ctx context.Context, addr string) (*models.Store, error) {
	result := models.Store{}
	session := r.pool.Session(nil)
	coll := session.DB(r.db).C(COLLECTION)
	err := coll.Find(bson.M{"direccion": addr}).One(&result)

	switch err {
	case nil:
		return &result, nil
	case mgo.ErrNotFound:
		return nil, models.ErrNotFound
	default:
		return nil, err
	}
}

func (r mongoStoreRepository) Store(ctx context.Context, s *models.Store) error {
	session := r.pool.Session(nil)
	coll := session.DB(r.db).C(COLLECTION)
	err := coll.Insert(s)
	if err != nil {
		return err
	}
	return  nil
}


