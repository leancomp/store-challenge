package repository

import (
	"context"
	"gitlab.com/leancomp/store-challenge/models"
)

type StoreRepository interface {
	GetAll(ctx context.Context) ([]*models.Store, error)
	GetByID(ctx context.Context, id models.ID) (*models.Store, error)
	GetByAddress(ctx context.Context, addr string) (*models.Store, error)
	Store(ctx context.Context, a *models.Store) error
}