package http

import (
	"context"
	"github.com/gin-gonic/gin"
	"github.com/sirupsen/logrus"
	"gitlab.com/leancomp/store-challenge/models"
	"gitlab.com/leancomp/store-challenge/usecase"
	validator "gopkg.in/go-playground/validator.v9"
	"net/http"
	"strconv"
)

type ResponseError struct {
	Message string `json:"message"`
}

type StoreHandler struct {
	Usecase usecase.Usecase
}

// Store godoc
// @Summary Add a store
// @Description add by json store
// @Accept  json
// @Produce  json
// @Param store body models.Store true "Add store"
// @Success 200 {object} models.Store
// @Router /store [post]
func (h *StoreHandler) Store(c *gin.Context) {
	var store models.Store
	err := c.Bind(&store)
	if err != nil {
		c.JSON(http.StatusUnprocessableEntity, err.Error())
		return
	}

	if ok, err := isRequestValid(&store); !ok && err != nil {
		c.JSON(http.StatusBadRequest, err.Error())
		return
	}
	ctx := c.Request.Context()
	if ctx == nil {
		ctx = context.Background()
	}

	err = h.Usecase.Store(ctx, &store)

	if err != nil {
		logrus.Error(err)
		c.JSON(getStatusCode(err), ResponseError{Message: err.Error()})
		return
	}
	c.JSON(http.StatusCreated, store)
}

func isRequestValid(s *models.Store) (bool, error) {
	validate := validator.New()
	err := validate.Struct(s)
	if err != nil {
		return false, err
	}
	return true, nil
}

// GetStore godoc
// @Summary Get store
// @Description get store by id
// @Accept  json
// @Produce  json
// @Param id path string true "Store ID"
// @Success 200 {array} models.Store
// @Router /store/{id} [get]
func (h *StoreHandler) GetByID(c *gin.Context) {
	idP := c.Param("id")

	if !models.IsValidID(idP) {
		c.JSON(http.StatusUnprocessableEntity, models.ErrBadParamInput.Error())
		return
	}
	idB := models.StringToID(idP)

	ctx := c.Request.Context()
	if ctx == nil {
		ctx = context.Background()
	}

	suc, err := h.Usecase.GetByID(ctx, idB)
	if err != nil {
		logrus.Error(err)
		c.JSON(getStatusCode(err), ResponseError{Message: err.Error()})
		return
	}
	c.JSON(http.StatusOK, suc)
}

// GetClosest godoc
// @Summary Closest
// @Description get closest store by position
// @Accept  json
// @Produce  json
// @Param lat query string true "Latitud"
// @Param lng query string true "Longitud"
// @Success 200 {array} models.Store
// @Router /stores/closest [get]
func (h *StoreHandler) GetClosest(c *gin.Context) {
	lat, errLat := strconv.ParseFloat(c.Query("lat"), 64)
	lng, errLng := strconv.ParseFloat(c.Query("lng"), 64)

	if errLat != nil || errLng != nil {
		c.JSON(http.StatusBadRequest, models.ErrBadParamInput.Error())
	}

	ctx := c.Request.Context()
	if ctx == nil {
		ctx = context.Background()
	}

	suc, err := h.Usecase.GetClosest(ctx, lat, lng)
	if err != nil {
		logrus.Error(err)
		c.JSON(getStatusCode(err), ResponseError{Message: err.Error()})
		return
	}
	c.JSON(http.StatusOK, suc)
}

func NewStoreHandler(r *gin.RouterGroup, us usecase.Usecase) {
	handler := &StoreHandler{
		Usecase: us,
	}
	s := r.Group("/store")
	{
		s.POST("/", handler.Store)
		s.GET("/:id", handler.GetByID)
	}

	ss := r.Group("/stores")
	{
		ss.GET("/closest", handler.GetClosest)
	}
}

func getStatusCode(err error) int {
	if err == nil {
		return http.StatusOK
	}
	logrus.Error(err)
	switch err {
	case models.ErrInternalServerError:
		return http.StatusInternalServerError
	case models.ErrNotFound:
		return http.StatusNotFound
	case models.ErrConflict:
		return http.StatusConflict
	default:
		return http.StatusInternalServerError
	}
}