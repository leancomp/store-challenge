# Store-Api Challenge

![](https://revive.run/images/gen/logo.png)

This is a pure conceptual sample, an exercise to show how to work with Golang in a clean-architecture way.

# Challenge
>Para mejorar la experiencia del cliente, la sucursal default que se ofrece para store pickup debe ser la más cercana a su ubicación. Para esto, una de las funcionalidades que se necesita es conocer la sucursal más cercana a un punto dado (latitud y longitud).
Diseñar e implementar un servicio que exponga en su API las operaciones CRUD (únicamente creación y lectura por id) de la entidad Sucursal y la consulta de sucursal más cercana a un punto dado.
Los campos de dicha entidad son id, dirección, latitud y longitud.
Diseñar e implementar (unit tests) al menos UN caso de prueba exitoso y uno no exitoso de dominio para la operación de creación de una sucursal.
Diseñar e implementar al menos UN unit test exitoso del cálculo de distancias.

# Topics that cover

  - Code your first API in a language that you don't know (GO in this case)
  - Apply *clean-arch* concepts
  - Persist all entities in MongoDB
  - Build and run all the stack with Docker-Compose
  - Swagger documentation for the API
  - Healthchecks for the db


### Building & running

We're using docker-compose for build an start the complete stack, but also have a ```Makefile``` for some operations like build locally, run test cases, rebuild src image, etc

```makefile
build:
	go build -o $(BINARY_NAME) -v
test:
	go test -v ./...
clean:
	go clean
	rm -f $(BINARY_NAME)
run:
	docker-compose up api
deps:
	go mod download
docker-compose-build:
	docker-compose up --build api
```

In the ```docker-compose.yml``` you also can switch between ```.env``` files for DEV, PROD, TEST, etc.

```sh
env_file:
      - ./dev.env
```

# API
In this "slice" of a real API we can found a half of CRUD operations for the resource ```store```.

## **`POST`** `/api/v1/store`
*Creates a new `store`*
**Request:**
````
curl -d '{
  "address": "Calle Falsa 123, Buenos Aires",
  "latitud": -34.6523907,
  "longitud": -58.4403929
}' 
-H "Content-Type: application/json" 
-X POST http://localhost:8080/api/v1/store
````
**Response:**
````
{
  "id": "5e3047f2519934000154c5c9"
  "address": "Calle Falsa 123, Buenos Aires",
  "latitud": -34.6523907,
  "longitud": -58.4403929
}
````

## **`GET`** `/api/v1/store/{id}`
*Retrieve the `store` with the id = `{id}`*
**Request:**
````
curl http://localhost:8080/api/v1/store/5e3047f2519934000154c5c9
````
**Response:**
```
{
  "id": "5e3047f2519934000154c5c9"
  "address": "Calle Falsa 123, Buenos Aires",
  "latitud": -34.6523907,
  "longitud": -58.4403929
}
```

## **`GET`** `/api/v1/stores/closest?lat={lat}&lng={lng}`
*Retrieve the closest `store` by the position provided. The parameters ***lat*** and ***lng*** should be float with `.` decimal separator*

**Request:**
````
curl http://localhost:8080/api/v1/stores/closest?lat=35.49467&lng=54.2342
````
**Response:**
```
{
  "id": "5e3047f2519934000154c5c9"
  "address": "Calle Falsa 123, Buenos Aires",
  "latitud": -34.6523907,
  "longitud": -58.4403929
}
```

## Swagger
We also have the API documentation in Swagger and you can found it in `http://localhost:8080/swagger/index.html`
