package models

type Store struct {
	ID       ID 			`json:"_id" bson:"_id"`
	Address  string			`json:"address" validate:"required" example:"Calle 123"`
	Latitud  float64		`json:"latitud" validate:"required"`
	Longitud float64		`json:"longitud" validate:"required"`
}